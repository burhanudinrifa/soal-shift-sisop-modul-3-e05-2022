#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <dirent.h>

void rm(char *path){
    FILE* ptr;
    char ch;
    struct dirent *entry = NULL;
    DIR *dp = NULL;

    dp = opendir(path);
    if (dp != NULL) {
        while ((entry = readdir(dp))){
            char filename[1024];
            sprintf(filename, "%s", entry->d_name);

            ptr = fopen(filename, "r");
            
            if(ptr == NULL){
                printf("Error : File Can't Be Opened\n");
            }

            else{
                printf("content of this file are \n");

                do{
                    ch = fgetc(ptr);
                    printf("%c", ch);
                    } 
                while (ch != EOF);
            }
 
        }
        fclose(ptr);  
    }

    closedir(dp);
}

int main(int argc, const char**argv) {
    rm("music");
}