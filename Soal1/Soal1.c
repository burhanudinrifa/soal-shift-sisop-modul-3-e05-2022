#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <dirent.h>
#include <string.h>

void make_dir(char* name){
    char *argv[] = {"mkdir", "-p", name, NULL};
    execv("/bin/mkdir", argv);
    exit(0);
}

void download(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt'", "-O", "Quote", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1'", "-O", "Music", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}

void unzip(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "Quote.zip", "-d", "quote", NULL};
        execv("/bin/unzip", argv);
        exit(0);
    } 
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"unzip", "Music.zip", "-d", "music", NULL};
        execv("/bin/unzip", argv);   
        exit(0);
    }
}

void iterate_directory(char *path){
    FILE* ptr;
    char ch;
    struct dirent *entry = NULL;
    DIR *dp = NULL;

    dp = opendir(path);
    if (dp != NULL) {
        while ((entry = readdir(dp))){
            char filename[1024];
            sprintf(filename, "%s", entry->d_name);

            ptr = fopen(filename, "r");
            
            if(ptr == NULL){
                printf("Error : File Can't Be Opened\n");
            }

            else{
                printf("content of this file are \n");

                do{
                    ch = fgetc(ptr);
                    printf("%c", ch);
                    } 
                while (ch != EOF);
            }
 
        }
        fclose(ptr);  
    }

    closedir(dp);
}

size_t decode(const char *in){
    size_t len;
    size_t tmp;
    size_t i;

    if (in == NULL){
        printf("Error : Variable \"*in\" is Empty\n");
        return 0;
    }
    
    len = strlen(in);
    tmp = len/4 * 3;

    for (i = len; i-->0; ) {
        if (in[i] == '=') {
			tmp--;
		}
        else {
			break;
		}
	}

	return tmp;
}

void download_n_unzip(){
    pid_t child;
    int status;

    // Download Zip
    child = fork();
    if(child < 0)
        exit(EXIT_FAILURE);

    if(child == 0){
        download();
    }

    else{
        while ((wait(&status)) > 0);

        // Unzip Downloads
        child = fork();
        if(child < 0)
            exit(EXIT_FAILURE);

        if(child == 0)
            unzip();

        else{

        }
    }
}

int main()
{
    pid_t child;
    int status;

    // Download and Unzip
    child = fork();
    if(child < 0)
        exit(EXIT_FAILURE);

    if(child == 0){
        // download_n_unzip();
        iterate_directory("music");
    }   
}